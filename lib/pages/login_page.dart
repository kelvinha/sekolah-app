import 'package:flutter/material.dart';
import 'package:sekolah_app_provider/config/theme.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isActiveUsername = false;
  bool isActivePassword = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
          left: 30,
          right: 30,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 100,
                    ),
                    Image.asset(
                      "assets/images/logo.png",
                      height: 268,
                      width: 205,
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              // username input
              Text(
                "Username",
                style: TextStyle(
                    fontFamily: 'Poppins',
                    color: isActiveUsername == true
                        ? primaryColor
                        : disabledTextColor),
              ),
              const SizedBox(
                height: 8,
              ),
              TextField(
                onTap: () {
                  setState(() {
                    isActiveUsername = true;
                  });
                },
                onTapOutside: (event) {
                  setState(() {
                    isActiveUsername = false;
                  });
                },
                onSubmitted: (value) {
                  setState(() {
                    isActiveUsername = false;
                  });
                },
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                cursorColor: primaryColor,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                    left: 20,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: disabledTextColor,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: primaryColor,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              // password input
              Text(
                "Password",
                style: TextStyle(
                    fontFamily: 'Poppins',
                    color: isActivePassword == true
                        ? primaryColor
                        : disabledTextColor),
              ),
              const SizedBox(
                height: 8,
              ),
              TextField(
                onTap: () {
                  setState(() {
                    isActivePassword = true;
                  });
                },
                onTapOutside: (event) {
                  setState(() {
                    isActivePassword = false;
                  });
                },
                onSubmitted: (value) {
                  setState(() {
                    isActivePassword = false;
                  });
                },
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                cursorColor: primaryColor,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                    left: 20,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: disabledTextColor,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: primaryColor,
                      width: 2,
                    ),
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Container(
                height: 50,
                width: double.infinity,
                decoration: const BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: disabledTextColor,
                      offset: Offset(
                        1.0,
                        1.0,
                      ),
                      blurRadius: 5.0,
                      spreadRadius: 2.0,
                    ),
                  ],
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                  color: secondaryColor,
                ),
                child: Center(
                  child: Text(
                    "Login",
                    style: boldPoppins,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
