import 'package:flutter/material.dart';

// color for regular
const Color primaryColor = Color(0xFF0A14CB);
const Color secondaryColor = Color(0xFFF1C809);
const Color disabledTextColor = Color(0xFFBEBEBE);
const Color baseColor = Color(0xFFFFFFFF);
// color for button
const Color successColor = Color(0xFF0EC361);
const Color failedColor = Color(0xFFB90101);

// text style
TextStyle regularPoppins = const TextStyle(
  fontFamily: 'Poppins',
  fontSize: 16,
);

TextStyle boldPoppins = const TextStyle(
  fontFamily: 'Poppins',
  fontSize: 16,
  fontWeight: FontWeight.bold,
);
