import 'package:flutter/material.dart';
import 'package:sekolah_app_provider/pages/login_page.dart';
import 'package:sekolah_app_provider/pages/test_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => const LoginPage(),
        '/test': (context) => const TestPage(),
      },
    );
  }
}
